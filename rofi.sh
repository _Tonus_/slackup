#!/bin/sh

title="<span size='x-large'><b>                          Slackware and friendly repos updater</b></span>"
#menu1="----------------------------------------------  Please choose relevent repos  ----------------------------------------------"
menu1="---------------------------------  Please choose relevent repos with shift+Enter  ---------------------------------"
choice="\
slackware64-current
ktown
alien_restricted
multilib
slackware-current
ktown_32
alien_32_restricted
ktown-testing
liveslack
libreoffice
slackware-live
HELP"
chk_dl="\
check
sync"
verbosities="\
show_progress
silent"

#rofi -monitor -4 -location 2 -e "$title"
#rofi -width 100 -e "<span color='black' background='white' size='xx-large'><b>                                                        $title</b></span>" -markup
rofi -location 2 -e "<span color='black' background='grey' size='x-large'><b>                          Slackware and friendly repos update                           </b></span>" -markup

selected=$(echo "$choice" | rofi -dmenu -multi-select -no-custom -p "$menu1" -mesg "$title")
list=$(echo $selected)

if [ -z "$selected" ]
then
exit 0
else
choice=$(echo "$chk_dl" | rofi -lines 1 -columns 2 -dmenu -no-custom -p "" -mesg "$title
Selected repos : $list")
fi

if [ -z $choice ]
then
exit 0
else
verb=$(echo "$verbosities" | rofi -lines 1 -columns 2 -dmenu -no-custom -p "" -mesg "$title")
fi
echo $verb
if [ $choice == "check" ]
then
perform="-c"
elif [ $choice == "check" ]
then
perform="-d"
else
exit 0
fi

if [ $verb = "show_progress" ]
then
verbosity="&&"
elif [ $verb = "silent" ]
then
verbosity="2>&1>/dev/null"
else
exit 0
fi
echo $verbosity
for repo in $selected
do
	case $repo in
		*slackware64-current*)
		../slack-repos-rsync.sh $perform"64" $verbosity
		;;
		*ktown*)
		../ktown-rsync.sh $perform"64" $verbosity
		;;
		*multilib*)
		../multilib-rsync.sh $perform $verbosity
		;;
		*alien_restricted*)
		../alien-repos-rsync.sh $perform"res64" $verbosity
		;;
		*slackware-current*)
		../slack-repos-rsync.sh $perform"32" $verbosity
		;;
		*alien_32_restricted*)
		../alien-repos-rsync.sh $perform"res32" $verbosity
		;;
		*ktown-testing_64*)
		../ktown-rsync.sh $perform"t64" $verbosity
		;;
		*ktown-testing_32*)
		../ktown-rsync.sh $perform"t32" $verbosity
		;;
		*liveslak*)
		../alien-repos-rsync.sh $perform"live" $verbosity
		;;
		*libreoffice*)
		../alien-repos-rsync.sh $perform"lo" $verbosity
		;;
		*slackware-live*)
#		../alien-slacklive-rsync.sh $verbosity
		flavours=$(echo "$live_flavours" | rofi -dmenu -multi-select -no-custom -p "$menu2" -mesg "$title")
		echo flavours
		;;
	esac
done

