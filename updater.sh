#!/bin/sh

rofi=$(which rofi 2> /dev/null)
dmenu=$(which dmenu 2> /dev/null)
dialog=$(which dialog 2> /dev/null)

rofi_updater () {
./rofi.sh
}

dmenu_updater () {
echo dmenu
}

dialog_updater () {
echo dialog
}

shell_updater () {
echo shell
}


if xset q &>/dev/null
then
    if [ $rofi ]
    then
        rofi_updater
    elif [ $dmenu ]
    then
        dmenu_updater
    elif [ $dialog ]
    then
        dialog_updater
    else
        exit 0
    fi
elif [ $dialog ]
then
    dialog_updater
else
    shell_updater
fi
